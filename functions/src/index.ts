import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import * as express from 'express';
import * as cors from 'cors';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
export const helloWorld = functions.https.onRequest((request, response) => {
  functions.logger.info("Call helloWorld!", {structuredData: true});
  response.json({
    mensaje: "Hola desde funciones en Firebase!"
  });
});

export const getGoty = functions.https.onRequest( async (request, response) => {
  functions.logger.info("call getGoty!", {structuredData: true});

  
  // const nombre = request.query.nombre || 'Sin nombre'; // "Carlitos"

  // response.json({
  //   nombre
  // });

  const gotyRef = db.collection('goty');
  // comando async y await induce a que la linea de codigo espera la rpta
  const docsSnap = await gotyRef.get();

  const juegos = docsSnap.docs.map( doc => doc.data());

  response.json(juegos);

});

// Servidor Express
const app = express();
app.use ( cors ({origin: true}));

app.get('/goty', async (req, res) => {
  const gotyRef = db.collection('goty');
  // comando async y await induce a que la linea de codigo espera la rpta
  const docsSnap = await gotyRef.get();

  const juegos = docsSnap.docs.map( doc => doc.data());

  res.json(juegos);
})

app.post('/goty/:id', async (req, res) => {
  const id = req.params.id;
  const gameRef = db.collection('goty').doc(id);
  const gameSnap = await gameRef.get();

  if (!gameSnap.exists) {
    res.status(404).json({
      ok: false,
      mensaje: 'No existe registro con el Id ingresado [' + id + ']'
    })
  } else {
    const antes = gameSnap.data() || {votos: 0}; // se añade || {votos: 0} para evitar validacion de "undefined"
    await gameRef.update({
      votos: antes.votos + 1
    });
    res.json({
      ok: true,
      mensaje: `Gracias por votar por [${ antes.name}]`
    });
  }
})

// para informar a firebase que existe express como servidor
export const api = functions.https.onRequest( app );
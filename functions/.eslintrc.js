module.exports = {
    root: true,
    env: {
        es6: true,
        node: true,
    },
    extends: [
        "eslint:recommended",
        "plugin:import/errors",
        "plugin:import/warnings",
        "plugin:import/typescript",
        "google",
        "plugin:@typescript-eslint/recommended"
    ],
    parser: "@typescript-eslint/parser",
    parserOptions: {
        project: ["tsconfig.json", "tsconfig.dev.json"],
        tsconfigRootDir: __dirname,
        sourceType: "module",
    },
    ignorePatterns: [
        "/lib/**/*", // Ignore built files.
    ],
    plugins: [
        "@typescript-eslint",
        "import",
    ],
    rules: {
        // quotes: ["error", "double"],
        "quotes": 0,
        "indent": 0,
        "eol-last": 0,
        "no-multiple-empty-lines": 0,
        "object-curly-spacing": 0,
        "spaced-comment": 0,
        "ignoreComments": 0,
        "ignoreStrings": 0,
        "max-len": "off",
        "require-jsdoc": 0,
        "space-before-blocks": 0,
        "padded-blocks": 0,
        "no-trailing-spaces": 0,
        "no-unused-vars": 0,
        "new-cap": 0,
        "comma-spacing": 0,
        "comma-dangle": 0,
        "key-spacing": 0,
        "func-call-spacing": 0,
        "block-spacing": 0,
        "semi": 0,
        "brace-style": 0,
        "arrow-parens": 0,
    },
};
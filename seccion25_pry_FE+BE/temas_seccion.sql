
-- para el backend en firebase
npm install -g firebase-tools
-- para comprobar la version
firebase --version

-- para autentificar. ejecutar de nuevo el mismo comando para validar la cuenta
firebase login

-- para cerrar sesion desde el terminal
firebase logout

-- para iniciar el proyecto, crear carpeta de trabajo y ubicarse dentro de en el terminal. Ejecutar:
firebase init


-- para actualizar dependencias en caso se interrumpa la instalacion
npm install firebase-functions@latest firebase-admin@latest --save
npm install -g firebase-tools

-- para desplegar en produccion, se tiene que estar en la carpeta raiz del proyecto
firebase deploy

-- para levantar servicio local tipo DESARROLLO
firebase serve

-- para compilar los cambios de tsc a js manualmente, ubicarse en la carpeta functions
cd functions
npm run build

-- para compilar automaticamente
cd functions
tsc --watch

-------------------------------------------------
-------------------------------------------------
-- error marciano 1: evitar errores en deploy

En el archivo .eslintcr si se tiene se agregan las siguientes reglas en off = 0, de lo contrarió se puede modificar la regla, pero por tiempo desactive las que me pueden dar problemas al desplegar.
Saludos

  rules: {     "quotes": 0,     "indent": 0,     "eol-last": 0,     "no-multiple-empty-lines": 0,     "object-curly-spacing": 0,     "spaced-comment": 0,     "ignoreComments": 0,     "ignoreStrings": 0,     "max-len": "off",     "require-jsdoc": 0,     "space-before-blocks": 0,     "padded-blocks": 0,        "no-trailing-spaces": 0,     "no-unused-vars": 0,     "new-cap": 0,     "comma-spacing": 0,     "comma-dangle" :0,     "key-spacing": 0,     "func-call-spacing": 0,     "block-spacing": 0,     "semi": 0,     "brace-style": 0,     "arrow-parens": 0,   },
-------------------------------------------------
-------------------------------------------------
-- error marciano 2: Parsing error: Cannot read file

By default, the projects (in parserOptions) are resolved relative to the current working directory. If you run eslint in a different working directory to the folder containing tsconfig.json, @typescript-eslint/parser will not be able to locate the file.

To fix this, you can set tsconfigRootDir to __dirname, which would make the parser resolve the project configuration relative to .eslintrc:

module.exports = {
  // ...
  parserOptions: {
    project: "tsconfig.json",
    tsconfigRootDir: __dirname,
    sourceType: "module",
  },
  // ...
}
-------------------------------------------------
-------------------------------------------------
-- para instalar express y cors (), moverse a la carpeta functions
cd functions
npm install express cors

-- instalar tipados. el comando "save=dev" indica que solo se usara en desarrollo y no subiran a produccion
cd functions
npm install @types/cors --save -dev
npm install @types/express --save -dev

-- luego de publicado express, se usa la url generada por firebase añadiendo el subdirectorio
http://localhost:5000/firestore-grafica-35ac6/us-central1/api + '/goty' ->
http://localhost:5000/firestore-grafica-35ac6/us-central1/api/goty


--- en frontend
https://sweetalert2.github.io/
npm install sweetalert2

-- en frontend
https://github.com/angular/angularfire

-- para compilar el frontend
ng build --prod

-- para subirlo a un hosting de firebase
--- solo para verificar que este la sesion conectada
firebase login 

--- ejecutar
firebase init

	-- directorio publico
	? What do you want to use as your public directory? /dist/gotyPryFE
	? Configure as a single-page app (rewrite all urls to /index.html)? Yes
	? Set up automatic builds and deploys with GitHub? No
	? File /dist/gotyPryFE/index.html already exists. Overwrite? No

-- deploy
firebase deploy
-- al generarse error, se cambia "public": "./dist/gotyPryFE" en el archivo firebase.json

-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
Código de la sección

En caso de que lo lleguen a necesitar, este es el código fuente de la sección:

GitHub - Aplicación de Angular
https://github.com/Klerith/angular-grafica-firestore

Github - Backend + Cloud Functions
https://github.com/Klerith/goty-backend-cloud-functions

Espero que esta sección les gustará mucho!


Si quieren más sobre mis cursos, pueden encontrarlos aquí

Sitio web de Fernando Herrera
https://fernando-herrera.com/#/home

Nos vemos en el próximo curso
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
Más sobre mis cursos

Más cursos

Si desean más cursos gratuitos, los tengo listados aquí para que los puedan tomar fácilmente

Fernando Herrera - Cursos gratuitos
https://fernando-herrera.com/#/cursos-gratis

Y también aquí pueden encontrar todos mis cursos al menor precio posible siempre, todo el año

Fernando Herrera - Cursos de pago al menor precio posible siempre.
https://fernando-herrera.com/#/home

Subir el certificado de Udemy:

Pueden subir su certificado que les genera Udemy a mi página web, y participar en promociones, regalos, otras cosas que se me ocurran, simplemente descarguen el certificado que les genera Udemy y subanlo aquí:

Logros de Alumnos - Fernando Herrera
https://fernando-herrera.com/#/logros

Nota:

Si pueden, usen los cupones de mi sitio web, eso me ayuda a mi y ustedes siempre lo tendrán al menor precio possible que puedo dejarlos $9.99


Espero que el curso fuera todo lo que esperaban y más!